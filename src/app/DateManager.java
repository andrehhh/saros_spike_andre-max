package app;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.sql.Date;

public class DateManager {
	Calendar calendar; 
	DateFormat df;
	
 	public DateManager()
	{
		this.calendar = Calendar.getInstance();
	}
	
	public DateManager(String date, int days) throws ParseException 
	{
		df = new SimpleDateFormat("YY-MM-DD"); 
		calendar = Calendar.getInstance();
		increaseDays(days);
	}
	
	public void increaseDays(int days) {
		calendar.add(Calendar.DATE, days);
	}
	
	public String getDate() {
		return df.format(calendar.getTime());
	}

	public void setDate(String date)
	{
		calendar.setTime(Date.valueOf(date));
	}

	
	public String getDateOfString(String date)
	{
		return df.format(date);
	}
}

