package app;

import java.util.Scanner;

public class InputManager {
	
	public static String getInputFromConsole(Scanner scanner)
	{
		return scanner.nextLine();
	}
}
