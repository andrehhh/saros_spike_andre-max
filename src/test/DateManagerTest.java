package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;
import app.DateManager;
public class DateManagerTest {

	DateManager dm;
	
	@Before public void setUp() {
		try {
			dm = new DateManager("2001-01-01", 0);	
		}
		catch (Exception e)
		{
			System.out.println("Could not create DateManager, Formatting error");
		}
	}
	
	@Test public void oneDayAfterSomeDayShouldReturnDayAfter()
	{
		assertEquals("2001-01-01" , dm.getDate());
		dm.increaseDays(2);
		assertEquals("2001-01-03", dm.getDate());
	}
	
	@Test public void shouldReturnSetDate()
	{
		dm.setDate("2002-02-02");
		assertEquals("2002-02-02", dm.getDate());
		
	}
	
	@Test public void shouldReturnDate()
	{
		assertEquals("2002-02-02", dm.getDateOfString("02-02-02"));
	}

	
	@Test public void shouldReturnNewYear()
	{
		dm.setDate("2017-12-31");
		dm.increaseDays(1);
		assertEquals("18-01-01", dm.getDate());
	}
	
	@Test public void shouldReturnXDaysLater()
	{
		int x = 2;
		dm.increaseDays(x);
		assertEquals("2001-01-03", dm.getDate());
	}
	
	@Test public void shouldCorrectlyGetToFeb29thOnLeapYear() {
		dm.setDate("2004-02-28");
		dm.increaseDays(1);
		assertEquals("2004-02-29", dm.getDate());
	}
	
	@Test(expected = ParseException.class)
	public void shouldThrowIfConstructedWithFaultyString() throws ParseException {
		new DateManager("2001/01/01", 0);
	}
}
